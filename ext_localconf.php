<?php
/**
 * Created by PhpStorm.
 * User: nabossha
 * Date: 28.07.2014
 * Time: 13:27
 */

defined('TYPO3_MODE') || die();

/**
 * Development environment
 * TYPO3_CONTEXT Development
 */
if(\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isDevelopment()) {
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['powermailDevelopContextEmail'] = 'formsammler@bosshartong.ch';
    // debugging / Logging:
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '172.16.0.*,213.193.81.194';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = true;
}

/**
 * Production/Staging environment
 * TYPO3_CONTEXT Production
 */
if(\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isProduction()) {
   //

}


/***************
 * set realurl configuration
 */
$realurlConfiguration = array(
    'configFile' => 'typo3conf/ext/boto_base/Resources/Private/php/realurl.conf.php',
    'enableAutoConf' => '0',
    'autoConfFormat' => '0',
    'enableDevLog' => '0',
    'moduleIcon' => '2',
);
$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['realurl'] = serialize($realurlConfiguration);


/***************
 * Devlog-Extension must be present:
 */
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('devlog')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['enable_errorDLOG'] = 1;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['enable_exceptionDLOG'] = 1;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['errorHandlerErrors'] = 0;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['exceptionalErrors'] = 0;
}

/***************
 * REGISTER TASKS
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Bosshartong\BotoBase\Tasks\CleanupTempFiles::class] = array(
    'extension'        => $_EXTKEY,
    'title'            => 'LLL:EXT:boto_base/Resources/Private/Language/locallang.xlf:tasks.cleanupTempfile.title',
    'description'      => 'LLL:EXT:boto_base/Resources/Private/Language/locallang.xlf:tasks.cleanupTempfile.description',
    'additionalFields' => 'Bosshartong\\BotoBase\\Tasks\\CleanupTempFilesAdditionalFields'
);

/**
 * add infos to system information toolbar
 */
if (TYPO3_MODE === 'BE') {
    $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
    /**
     * Add current Bootstrap Package version to system information toolbar
     */
    $signalSlotDispatcher->connect(
        \TYPO3\CMS\Backend\Backend\ToolbarItems\SystemInformationToolbarItem::class,
        'getSystemInformation',
        \Bosshartong\BotoBase\Backend\ToolbarItem\VersionToolbarItem::class,
        'addVersionInformation'
    );
}



?>
