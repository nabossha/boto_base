<?php

namespace Bosshartong\BotoBase\Tasks;

class CleanupTempFilesAdditionalFields implements \TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface {

    /**
     * This method is used to define new fields for adding or editing a task
     * In this case, it adds an email field
     *
     * @param	array					$taskInfo: reference to the array containing the info used in the add/edit form
     * @param	object					$task: when editing, reference to the current task object. Null when adding.
     * @param	\TYPO3\CMS\Scheduler\Controller\SchedulerModuleController		$parentObject: reference to the calling object (Scheduler's BE module)
     * @return	array					Array containg all the information pertaining to the additional fields
     *									The array is multidimensional, keyed to the task class name and each field's id
     *									For each field it provides an associative sub-array with the following:
     *										['code']		=> The HTML code for the field
     *										['label']		=> The label of the field (possibly localized)
     *										['cshKey']		=> The CSH key for the field
     *										['cshLabel']	=> The code of the CSH label
     */
    public function getAdditionalFields(array &$taskInfo, $task, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject) {
        // Initialize Dir-Selector
        if (empty($taskInfo['dir'])) {
            $taskInfo['dir'] = array();
            if ($parentObject->CMD == 'add') {
                // In case of new task, set to pics if it's available

                $taskInfo['dir'][] = '';

                $preselectedDirs = array('pics','tmb-files');

                foreach ($preselectedDirs as $preselectedDir) {
                    if (in_array($preselectedDir, $this->getRegisteredDirs())) {
                        $taskInfo['dir'][] .= $preselectedDir;
                    }
                }

            } elseif ($parentObject->CMD == 'edit') {
                // In case of editing the task, set to currently selected value
                $taskInfo['dir'] = $task->selectedDirs;
            }
        }

        $fieldSizeDirs = count($this->getRegisteredDirs());
        $fieldNameDirs = 'tx_scheduler[dir][]';
        $fieldIdDirs = 'task_dirs';
        if(!is_array($taskInfo['dir'])) {
            $taskInfo['dir'] = array();
        }
        $fieldOptionsDirs = $this->getCacheDirOptions($taskInfo['dir']);
        $fieldHtmlDirs =
            '<select name="' . $fieldNameDirs . '" id="' . $fieldIdDirs . '" class="wide" size="'.$fieldSizeDirs.'" multiple="multiple">' .
            $fieldOptionsDirs .
            '</select>';

        $additionalFields[$fieldIdDirs] = array(
            'code' => $fieldHtmlDirs,
            'label'    => 'Ordner innerhalb von /typo3temp',
            'cshKey' => '_MOD_tools_txschedulerM1',
            'cshLabel' => $fieldIdDirs,
        );

        // Initialize Table-Selector
        if (empty($taskInfo['table'])) {
            $taskInfo['table'] = array();
            if ($parentObject->CMD == 'add') {
                $taskInfo['table'][] = '';

            } elseif ($parentObject->CMD == 'edit') {
                // In case of editing the task, set to currently selected value
                $taskInfo['table'] = $task->selectedTables;
            }
        }

        $fieldSizeTables = count($this->getRegisteredTables());
        $fieldNameTables = 'tx_scheduler[table][]';
        $fieldIdTables = 'task_tables';
        $fieldOptionsTables = $this->getCacheTablesOptions($taskInfo['table']);
        $fieldHtmlTables =
            '<select name="' . $fieldNameTables . '" id="' . $fieldIdTables . '" class="wide" size="'.$fieldSizeTables.'" multiple="multiple">' .
            $fieldOptionsTables .
            '</select>';

        $additionalFields[$fieldIdTables] = array(
            'code' => $fieldHtmlTables,
            'label'    => 'Caching- und Log-Tabellen',
            'cshKey' => '_MOD_tools_txschedulerM1',
            'cshLabel' => $fieldIdTables,
        );

        return $additionalFields;

    }


    /**
     * This method checks any additional data that is relevant to the specific task
     * If the task class is not relevant, the method is expected to return true
     *
     * @param	array					$submittedData: reference to the array containing the data submitted by the user
     * @param	\TYPO3\CMS\Scheduler\Controller\SchedulerModuleController		$parentObject: reference to the calling object (Scheduler's BE module)
     * @return	boolean					True if validation was ok (or selected class is not relevant), false otherwise
     */
    public function validateAdditionalFields(array &$submittedData, \TYPO3\CMS\Scheduler\Controller\SchedulerModuleController $parentObject) {
        $submittedData['dir'] = $submittedData['dir'];
        $submittedData['table'] = $submittedData['table'];
        return true;
    }


    /**
     * This method is used to save any additional input into the current task object
     * if the task class matches
     *
     * @param	array				$submittedData: array containing the data submitted by the user
     * @param	\TYPO3\CMS\Scheduler\Task\AbstractTask	$task: reference to the current task object
     * @return	void
     */
    public function saveAdditionalFields(array $submittedData, \TYPO3\CMS\Scheduler\Task\AbstractTask $task) {
        $task->selectedDirs = $submittedData['dir'];
        $task->selectedTables = $submittedData['table'];
    }


    /**
     * Build select options of available dirs and set currently selected dirs
     *
     * @param array $dir Selected dirs
     * @return string HTML of selectbox options
     */
    protected function getCacheDirOptions(array $dir) {
        $optionsDirs = array();

        $availableDirs = $this->getRegisteredDirs();
        foreach ($availableDirs as $dirName) {
            if (in_array($dirName, $dir)) {
                $selected = ' selected="selected"';
            } else {
                $selected = '';
            }
            $optionsDirs[] =
                '<option value="' . $dirName .  '"' . $selected . '>' .
                $dirName .
                '</option>';
        }

        return implode('', $optionsDirs);
    }

    /**
     * Get all temp-dirs
     *
     * @return array temp-dirs
     */
    protected function getRegisteredDirs() {
        $dirs = array();
        $pathSite = PATH_site;
        $strDir  = $pathSite.'typo3temp/';
        $arDirs = scandir($strDir);

        foreach ($arDirs as $dir){
            if (preg_match('/(var)/',$dir)) continue;
            if (preg_match('/(.remove)/',$dir)) continue;
            if ($dir === '.' or $dir === '..') continue;
            if (is_dir($strDir . '/' . $dir)) {
                array_push($dirs, $dir);
            }
        }

        array_push($dirs, '*.remove');
        return $dirs;
    }



    /**
     * Build select options of available tables and set currently selected tables
     *
     * @param array $table Selected tables
     * @return string HTML of selectbox options
     */
    protected function getCacheTablesOptions(array $table) {
        $optionsTables = array();

        $availableTables = $this->getRegisteredTables();
        foreach ($availableTables as $tableName) {
            if (in_array($tableName, $table)) {
                $selected = ' selected="selected"';
            } else {
                $selected = '';
            }
            $optionsTables[] =
                '<option value="' . $tableName .  '"' . $selected . '>' .
                $tableName .
                '</option>';
        }

        return implode('', $optionsTables);
    }

    /**
     * Get all tables
     *
     * @return array tables
     */
    protected function getRegisteredTables() {
        $arTables = array();

        $tables = $GLOBALS['TYPO3_DB']->sql_query(
            'SHOW TABLES'
        );

        // explicitly whitelisted tables - ATTENTION:will be evaluated as REGEX!
        $whiteListedTables = array(
            '^cache_*'              // caching framework tables
        ,'^cf_*'                // caching framework tables
        ,'(?<!^sys)_log'        // all _log tables except sys_log
        ,'^index_stat_*'        // indexed search tables
        ,'fe_session_data'
        ,'fe_sessions'
        ,'_stat_word'           // ke_search tables
        ,'_stat_search'         // ke_search tables
        ,'^tx_realurl_*'
        );
        foreach ($whiteListedTables as $whiteListedTable)
        {
            $grouped_Whitelisting[] = "(" . $whiteListedTable . ")";
        }
        $master_WhitelistingPattern = implode($grouped_Whitelisting, "|");
        //debug($master_WhitelistingPattern);
        // build the list of available tables:
        if ($GLOBALS['TYPO3_DB']->sql_error()) {
            debug($GLOBALS['TYPO3_DB']->sql_error(), '$GLOBALS[TYPO3_DB]->sql_error()', __LINE__, __FILE__);
        }else{
            while ($row = $tables->fetch_row()) {
                if (preg_match ('/'.$master_WhitelistingPattern.'/i', $row[0]))
                {
                    array_push($arTables, $row[0]);
                }
            }
        }
        return $arTables;
    }
}