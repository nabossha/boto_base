<?php

namespace Bosshartong\BotoBase\Tasks;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Error\Exception;
use TYPO3\CMS\Core\Database\DatabaseConnection;
/* for V9 to be relaced with:
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
*/

class CleanupTempFiles extends \TYPO3\CMS\Scheduler\Task\AbstractTask {

    /**
     * Dirs that should be cleaned up,
     * set by additional field provider.
     *
     * @var array Selected dirs to delete files in there
     */
    public $selectedDirs = array();

    /**
     * Tables that should be cleaned up,
     * set by additional field provider.
     *
     * @var array Selected tables to clean
     */
    public $selectedTables = array();

    /**
     * Delete temp files and database tables, called by scheduler.
     *
     * @return boolean
     */
    public function execute() {

        if (is_array($this->selectedDirs)) {
            foreach ($this->selectedDirs as $selectedDir) {

                if ($selectedDir == '*.remove') {
                    $iterator = new \RecursiveIteratorIterator(
                        new \RecursiveDirectoryIterator(PATH_site . 'typo3temp/',
                            \RecursiveDirectoryIterator::SKIP_DOTS),
                        \RecursiveIteratorIterator::CHILD_FIRST
                    );
                    foreach ($iterator as $objname => $iterobj) {
                        if (is_dir($objname) && preg_match("/(.remove)/", $objname)) {
                            if (GeneralUtility::rmdir($objname, true)) {
                            } else {
                                throw new Exception(
                                    'Fehler: ' . $objname . "\n",
                                    1308270454
                                );
                            }
                        }
                    }
                } else {
                    $strDir  = PATH_site.'typo3temp/'.$selectedDir;
                    foreach (glob($strDir) as $strDirectory) {
                        debug($strDirectory);
                        if (is_dir($strDirectory)) {
                            if (GeneralUtility::rmdir($strDirectory, TRUE)) {
                            } else {
                                throw new Exception(
                                    'Fehler: '.$strDirectory."\n",
                                    1308270454
                                );
                            }
                        }
                    }

                }
            }
        }

        if (is_array($this->selectedTables)) {
            foreach ($this->selectedTables as $table ) {
                $this->truncateTable ($table);
            }
        }

        return true;

    }
    public function getAdditionalInformation() {

        $returnDirs = '';
        $returnTables = '';

        if (is_array($this->selectedDirs)) {
            $returnDirs = 'bereinigte Ordner: '. implode(', ', $this->selectedDirs) .'.';
        }

        if (is_array($this->selectedTables)) {
            $returnTables = 'bereinigte Tabellen: '. implode(', ', $this->selectedTables) .'.';
        }

        return $returnDirs."\n".$returnTables;

    }

    /**
     * @return DatabaseConnection
     */
    public function getDb() {
        return $GLOBALS ['TYPO3_DB'];
    }


    /**
     * @param string $table
     * @throws Exception
     */
    private function truncateTable($table) {
        if (FALSE === $this->getDb ()->exec_TRUNCATEquery ( $table )) {
            throw new Exception ('Truncate of table ' . $table . ' failed ' );
        }
    }
}
?>