<?php
/**
 * Created by PhpStorm.
 * User: nabossha
 * Date: 27.10.2017
 * Time: 19:01
 */

namespace Bosshartong\BotoBase\Backend\ToolbarItem;

use TYPO3\CMS\Backend\Backend\ToolbarItems\SystemInformationToolbarItem;
use TYPO3\CMS\Core\Utility\CommandUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * VersionToolbarItem
 */
class VersionToolbarItem
{
    /**
     * Called by the system information toolbar signal/slot dispatch.
     *
     * @param SystemInformationToolbarItem $systemInformation
     */
    public function addVersionInformation(SystemInformationToolbarItem $systemInformation)
    {
        $value = null;
        $extensionDirectory = ExtensionManagementUtility::extPath('boto_base');

        // Try to get current version from git
        if (file_exists($extensionDirectory . '.git')) {
            CommandUtility::exec('git --version', $_, $returnCode);
            if ((int)$returnCode === 0) {
                $currentDir = getcwd();
                chdir($extensionDirectory);
                $tag = trim(CommandUtility::exec('git tag -l --points-at HEAD'));
                if ($tag) {
                    $value = $tag;
                } else {
                    $branch = trim(CommandUtility::exec('git rev-parse --abbrev-ref HEAD'));
                    $revision = trim(CommandUtility::exec('git rev-parse --short HEAD'));
                    $value = $branch . ', ' . $revision;
                }
                chdir($currentDir);
            }
        }

        // Fallback to version from extension manager
        if (!$value) {
            $value = ExtensionManagementUtility::getExtensionVersion('boto_base');
        }

        // Set system information entry
        $systemInformation->addSystemInformation(
            htmlspecialchars('boto_base'),
            htmlspecialchars($value),
            'systeminformation-botobase'
        );
/*
        // Set system information entry
        $systemInformation->addSystemInformation(
            htmlspecialchars('GitLab commit#'),
            htmlspecialchars($value),
            'systeminformation-botobase'
        );
*/
    }
}