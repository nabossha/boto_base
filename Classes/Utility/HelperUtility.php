<?php
namespace Bosshartong\BotoBase\Utility;

/**
 * Helper classes for the 'boto_base' extension.
 *
 * @category    Utility
 * @package     TYPO3
 * @package     Bosshartong\BotoBase\Utility
 * @license     http://www.gnu.org/copyleft/gpl.html
 * #credits     https://github.com/geoffroy-aubry/Helpers/blob/stable/src/GAubry/Helpers/Helpers.php
 */

class HelperUtility
{

    /**
     * @param array $array1
     * @param array $array2
     * @return array
     */
    static public function ArrayMergeRecursiveDistinct(array &$array1, array &$array2) {
        $aMerged = $array1;

        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset ($aMerged [$key]) && is_array($aMerged [$key])) {
                $aMerged [$key] = self::ArrayMergeRecursiveDistinct($aMerged [$key], $value);
            } else {
                $aMerged [$key] = $value;
            }
        }

        return $aMerged;
    }
    /**
     * Flatten a multidimensional array (keys are ignored).
     *
     * @param array $aArray
     * @return array a one dimensional array.
     * @see http://stackoverflow.com/a/1320156/1813519
     */
    public static function flattenArray(array $aArray)
    {
        $aFlattened = array();
        array_walk_recursive(
            $aArray,
            function ($mValue) use (&$aFlattened) {
                $aFlattened[] = $mValue;
            }
        );
        return $aFlattened;
    }
}

