<?php
/**
 *
 * Real Url Configuration
 *
 * global realurl conf
 *
 * */

// write global configuration

$TYPO3_CONF_VARS['EXTCONF']['realurl']['_DEFAULT'] = array(

    'init' => array(
        'enableCHashCache' => TRUE,
        'enableUrlDecodeCache' => TRUE,
        'enableUrlEncodeCache' => TRUE,
        'adminJumpToBackend' => TRUE,
        'emptyUrlReturnValue' => '/',
        'appendMissingSlash' => 'ifNotFile',
        'postVarSet_failureMode' => '',
    ),

    'preVars' => array(),

    'pagePath' => array(
        'type' => 'user',
        'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
        'spaceCharacter' => '-',
        'languageGetVar' => 'L',
        'expireDays' => 3,
        'dontResolveShortcuts' => TRUE,
        'segTitleFieldList' => 'alias,title,subtitle,uid',
        'rootpage_id' => 1,
        'firstHitPathCache' => TRUE,
    ),

    'fileName' => array(
        'defaultToHTMLsuffixOnPrev' => FALSE,
        'index' => array(),
    ),

    'fixedPostVars' => array(),

    'postVarSets' => array(),
);