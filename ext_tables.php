<?php

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}
/***************
 * default values for backend styling
 */
$boto_baseConfArr = array(
    'envStyles' => 'env-prod',
    'loginBackgroundImage' => 'typo3conf/ext/boto_base/Resources/Public/Images/background.jpg',
);
/**
 * Development environment
 * TYPO3_CONTEXT Development
 */
if(\TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isDevelopment()) {
    $boto_baseConfArr['envStyles'] = 'env-dev';
    $boto_baseConfArr['loginBackgroundImage'] = 'typo3conf/ext/boto_base/Resources/Public/Images/background-dev.jpg';
}
/***************
 * assemble / apply backend styling
 * configure the backend, sets the login image and color
 */
$backendConfiguration = array(
    'loginLogo' => 'typo3conf/ext/boto_base/Resources/Public/Images/login-logo-alternate.svg',
    'loginHighlightColor' => '#375172',
    'loginBackgroundImage' => $boto_baseConfArr['loginBackgroundImage'],
    'backendLogo' => '',
    'backendFavicon' => '',
);
$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'] = serialize($backendConfiguration);

// add env-context to sitename:
$currentApplicationContext = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext();
$GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] .= ' (' . (string)$currentApplicationContext . ')';

// Register as a skin
if (TYPO3_MODE=="BE" ) {
    $TBE_STYLES['skins'][$_EXTKEY]['name']                                    = $_EXTKEY;
    $TBE_STYLES['skins'][$_EXTKEY]['stylesheetDirectories']['css']            = 'EXT:' . $_EXTKEY . '/Resources/Public/Styles/Backend/';
    $TBE_STYLES['skins'][$_EXTKEY]['stylesheetDirectories']['env']            = 'EXT:' . $_EXTKEY . '/Resources/Public/Styles/Backend/'.$boto_baseConfArr['envStyles'].'/';
}

/***************
 * Register Icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'boto-content-tab',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:boto_base/Resources/Public/Icons/ContentElements/tab.svg']
);
$iconRegistry->registerIcon(
    'systeminformation-botobase',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:boto_base/Resources/Public/Icons/SystemInformation/botobase.svg']
);

?>